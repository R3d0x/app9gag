import { Component, OnInit, Input } from '@angular/core';
import { VotesService } from '../services/votes/votes.service';
import { CommentsService } from '../services/comments/comments.service';
import { UsersService } from '../services/users/users.service';
import { ImagesService } from '../services/images/images.service';
import { PostsComponent } from '../posts/posts.component';

@Component({
    selector: 'app-post',
    templateUrl: './post.component.html',
    styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {

    @Input('image') imageDetails;
    @Input() posts: PostsComponent;
    score;
    comments = [];
    alreadyVoted = false;
    author: string;

    constructor(private _votesService: VotesService,
        private _commentsService: CommentsService,
        private _userService: UsersService,
        private _imageService: ImagesService,
        private _postsComponent: PostsComponent) { }

    ngOnInit() {
        this._votesService.fetchImageScore(this.imageDetails.id)
            .subscribe((data) => { this.score = (data['score'] != null ? data['score'] : '0'); });
        this._commentsService.fetchImageComments(this.imageDetails.id)
            .subscribe((data) => { this.comments = data; });
        this._userService.getUsernameById(this.imageDetails.idUtilisateur).then(authorName => {
            this.author = authorName;
        });
    }

    downVote() {
        if (!this.alreadyVoted && this._userService.logged) {
            this._votesService.addVote(this.imageDetails.id, false, this._userService.getUserId())
                .subscribe((data) => { this.score--; this.alreadyVoted = true; });
        }
    }

    upVote() {
        if (!this.alreadyVoted && this._userService.logged) {
            this._votesService.addVote(this.imageDetails.id, true, this._userService.getUserId())
                .subscribe((data) => { this.score++; this.alreadyVoted = true; });
        }
    }

    deletePost() {
        if (this.isUserPostAuthor()) {
            this._imageService.deleteImage(this.imageDetails.id).then(() => {
                this._postsComponent.removeImage(this.imageDetails.id);
            });
            this._commentsService.deleteCommentsOnImage(this.imageDetails.id)
                .subscribe();
            this._votesService.deleteVotesOnImage(this.imageDetails.id)
                .subscribe();
        }
    }

    isUserPostAuthor() {
        return this._userService.logged && this.imageDetails.idUtilisateur === this._userService.getUserId();
    }
}
