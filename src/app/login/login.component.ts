import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { UsersService } from '../services/users/users.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  form: FormGroup;
  error: boolean;

  constructor(private _usersService: UsersService, private _router: Router) {
    this.form = new FormGroup({
      login: new FormControl(),
      password: new FormControl(),
      passwordConfirmation: new FormControl()
    });
    this.error = false;
  }

  ngOnInit() {
  }

  submit(form: any) {
    this._usersService.login(this.form.get('login').value, this.form.get('password').value).then(() => {
      this.error = false;
      this._router.navigate(['/']);
    }).catch(() => {
      this.error = true;
    });
  }

}
