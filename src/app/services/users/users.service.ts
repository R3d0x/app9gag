import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppConfig } from '../../app.config';
import { Observable } from 'rxjs/Observable';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

@Injectable()
export class UsersService {

    private usersUrl: string;
    private usersPort = '8040';
    private token: string;
    public logged: boolean;
    private username: string;

    constructor(private _http: HttpClient, private config: AppConfig, private _router: Router) {
        const host: string = this.config.getConfig('host');
        this.usersUrl = host + ':' + this.usersPort + '/UserAccount/api/';
        if (this.username === undefined) {
            this.getUsernameById(this.getUserId()).then(username => {
                this.username = username;
                this.updateLogged();
            });
        }
    }

    updateLogged(): Promise<void> {
        const formData: FormData = new FormData();
        formData.append('username', this.username);
        formData.append('token', this.getToken());

        return new Promise((resolve, reject) => {
            if (this.getToken() === null) {
                reject();
                return;
            }
            this._http.post(this.usersUrl + 'connexion/isauth',
                // { 'username': this.getUsername(), 'token': this.token }).subscribe(({ isAuth }: any) => {
                formData).subscribe((res: { isAuth: any }) => {
                    this.logged = (res.isAuth === true) || (res.isAuth === 'true');
                    if (this.logged) {
                        resolve();
                    } else {
                        reject();
                    }
                });
        });

    }

    login(username, password) {
        return new Promise((resolve, reject) => {
            this._http.post(this.usersUrl + 'connexion/auth',
                { 'username': username, 'password': btoa(password) })
                .toPromise().then((res) => {
                    if (res['error']) {
                        reject();
                        return;
                    }
                    this.token = res['token'];
                    this.username = username;
                    localStorage.setItem('token', res['token']);
                    localStorage.setItem('userID', res['id']);

                    this.updateLogged();

                    resolve();
                });
        });
    }

    disconnect() {
        const formData: FormData = new FormData();
        formData.append('username', this.username);
        formData.append('token', this.getToken());

        this._http.post(this.usersUrl + 'connexion/decon',
            // { 'username': this.getUsername(), 'token': this.token }).subscribe(({ isAuth }: any) => {
            formData).toPromise().then(({ isAuth }: any) => {
                localStorage.removeItem('token');
                localStorage.removeItem('userID');
                this.logged = false;
                this._router.navigate(['/']);
            }).catch(() => {
                // Répétition en attendant la finalisation de l'API user
                localStorage.removeItem('token');
                localStorage.removeItem('userID');
                this._router.navigate(['/']);
                this.logged = false;
            });
    }

    createUser(username, password) {
        const formData: FormData = new FormData();
        formData.append('username', username);
        formData.append('password', btoa(password));

        this._http.post(this.usersUrl + 'user/add',
            // { 'username': this.getUsername(), 'token': this.token }).subscribe(({ isAuth }: any) => {
            formData).subscribe(({ isAuth }: any) => {
                this.login(username, password);
                this._router.navigate(['/']);
            });
    }

    getUsernameById(id: string | number): Promise<string> {
        return new Promise((resolve, reject) => {
            this._http.get(this.usersUrl + 'user/' + id).toPromise().then(({ fetchedId, username }: any) => {
                resolve(username);
            }).catch(() => {
                resolve('Anonymous');
            });
        });
    }

    getToken() {
        return localStorage.getItem('token');
    }

    getUserId() {
        return Number(localStorage.getItem('userID'));
        // https://stackoverflow.com/a/40958850/3670132
        // function hash(str) {
        //     let strHash = 0, i, chr, len;
        //     if (str.length === 0) { return strHash; }
        //     for (i = 0, len = str.length; i < len; i++) {
        //         chr = str.charCodeAt(i);
        //         strHash = ((strHash << 5) - strHash) + chr;
        //         strHash |= 0;
        //     }
        //     return strHash;
        // }
        // return hash(this.getUsername());
    }

    registerUser(username, password) {
        return this._http.post(this.usersUrl + 'user/add',
            { username, password })
            .subscribe((token: string) => { this.token = token; });
    }

    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<boolean> | Promise<boolean> | boolean {

        return new Promise((resolve, reject) => {
            this.updateLogged().then(() => { resolve(true); }).catch(() => {
                resolve(false);
                this._router.navigate(['/login']);
            });
        });
    }

}
