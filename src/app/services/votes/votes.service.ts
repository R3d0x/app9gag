import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment'
import { AppConfig } from '../../app.config';
//const environment = require('../../../environments/environment.json');

@Injectable()
export class VotesService {

    private votesUrl: string
    private votesPort = "8050";

    constructor(private _http: HttpClient, private config: AppConfig) {
        let host: string = this.config.getConfig("host");
        this.votesUrl = host + ":" + this.votesPort + "/votegag/images/"
    }

    fetchImageScore(imageId: string): Observable<any> {
        return this._http.get(this.votesUrl + imageId + "/score");
    }
    
    addVote(imageId, isUpVote, userId): Observable<any> {
        return this._http.post(this.votesUrl + imageId + "/votes", {"imageId":imageId, "isUpVote":isUpVote, "userId":userId});
    }

    deleteVotesOnImage(imageId): Observable<any> {
        return this._http.delete(this.votesUrl + imageId + "/votes");
    }
}