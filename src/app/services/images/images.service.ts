import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
// const environment = require('../../../environments/environment.json');
import { environment } from '../../../environments/environment';
import { AppConfig } from '../../app.config';
import { UsersService } from '../users/users.service';

@Injectable()
export class ImagesService {

    private imagesUrl: string;
    private _backendURL: any;
    private sizePage: any = 4;
    private imagesPort = 8030;

    constructor(private _http: HttpClient, private config: AppConfig, private _userService: UsersService) {
        const host: string = this.config.getConfig('host');
        this.imagesUrl = host + ':' + this.imagesPort + '/GPImages/api/images';
    }

    fetchImages(numPage: string, sortProperty: string): Observable<any> {
        return this._http.get(this.imagesUrl + '?page=' + numPage + '&size=' + this.sizePage + '&sort=' + sortProperty);
    }

    fetchImageDetails(imageId: string): Observable<any> {
        return this._http.get(this.imagesUrl + '/' + imageId);
    }

    addImage(form, image): Observable<any> {
        const dateObj = new Date();
        const year = dateObj.getUTCFullYear();
        const month = dateObj.getUTCMonth() + 1;
        const day = dateObj.getUTCDate();

        return this._http.post(this.imagesUrl,
            {
                'date': `${year}-${month}-${day}`,
                'file': image.split(',')[1],
                'idUtilisateur': this._userService.getUserId(),
                'titre': form.titreImg
            }
        );
    }

    deleteImage(id): Promise<Object> {
        return this._http.delete(this.imagesUrl + `/${id}`).toPromise();
    }
}
