import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppConfig } from '../../app.config';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class CommentsService {

    private commentsUrl: string
    private commentsPort = "8020";

    constructor(private _http: HttpClient, private config: AppConfig) {
        let host: string = this.config.getConfig("host");
        this.commentsUrl = host + ":" + this.commentsPort + "/Comments/images/";
    }

    fetchImageComments(imageId: string): Observable<any> {
        return this._http.get(this.commentsUrl + imageId + "/comments");
    }

    addComment(content, imageId, reply, userId): Observable<any> {
        return this._http.post(this.commentsUrl + imageId + "/comments",
            { "content": content, "reply": reply, "userId": userId }
        );
    }

    deleteComment(imageId, commentId) : Observable<any> {
        return this._http.delete(this.commentsUrl + imageId + "/comments/" + commentId);
    }

    deleteCommentsOnImage(imageId): Observable<any> {
        return this._http.delete(this.commentsUrl + imageId + "/comments");
    }
}
