import { RouterModule, Routes } from '@angular/router';

// APP COMPONENTS
import { RegistrationComponent } from './registration/registration.component';
import { AppComponent } from './app.component';
import { PostsComponent } from './posts/posts.component';
import { PostDetailsComponent } from './post-details/post-details.component';
import { UploadComponent } from './upload/upload.component';
import { LoginComponent } from './login/login.component';
import { UsersService } from './services/users/users.service';

export const ROUTES: Routes = [
    { path: '', redirectTo: '/images', pathMatch: 'full' },
    { path: 'images', component: PostsComponent },
    { path: 'upload', component: UploadComponent, canActivate: [UsersService] },
    { path: 'images/:id', component: PostDetailsComponent },
    { path: 'registration', component: RegistrationComponent },
    { path: 'login', component: LoginComponent }

];

export const APP_ROUTES = RouterModule.forRoot(ROUTES, { useHash: true });
