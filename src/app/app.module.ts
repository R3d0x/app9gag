import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { AppConfig } from './app.config';
import { APP_INITIALIZER } from '@angular/core';

import { VotesService } from './services/votes/votes.service';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

import { APP_ROUTES } from './app.routes';

import {
    MatToolbarModule,
    MatCardModule,
    MatTabsModule,
    MatButtonModule,
    MatInputModule,
    MatCheckboxModule,
    MatRadioModule,
    MatIconModule,
    MatListModule,
    MatDialogModule
} from '@angular/material';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { RegistrationComponent } from './registration/registration.component';
import { PostsComponent } from './posts/posts.component';
import { ImagesService } from './services/images/images.service';
import { PostComponent } from './post/post.component';
import { PostDetailsComponent } from './post-details/post-details.component';
import { CommentsService } from './services/comments/comments.service';
import { UploadComponent } from './upload/upload.component';
import { UsersService } from './services/users/users.service';
import { LoginComponent } from './login/login.component';
import { CommentComponent } from './comment/comment.component';

@NgModule({
    declarations: [
        AppComponent,
        ToolbarComponent,
        RegistrationComponent,
        PostsComponent,
        PostComponent,
        PostDetailsComponent,
        UploadComponent,
        LoginComponent,
        CommentComponent
    ],
    imports: [
        NgbModule.forRoot(),
        APP_ROUTES,
        MatToolbarModule,
        MatCardModule,
        MatTabsModule,
        MatButtonModule,
        MatInputModule,
        MatCheckboxModule,
        MatRadioModule,
        MatIconModule,
        MatListModule,
        MatDialogModule,
        BrowserModule,
        FormsModule,
        HttpClientModule,
        InfiniteScrollModule,
        ReactiveFormsModule
    ],
    providers: [
        VotesService,
        ImagesService,
        CommentsService,
        UsersService,
        AppConfig,
        { provide: APP_INITIALIZER, useFactory: (config: AppConfig) => () => config.load(), deps: [AppConfig], multi: true }
    ],
    bootstrap: [AppComponent],
    entryComponents: [AppComponent]
})
export class AppModule { }
