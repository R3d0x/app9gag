import { Component, OnInit } from '@angular/core';
import { UsersService } from '../services/users/users.service';

@Component({
    selector: 'app-toolbar',
    templateUrl: './toolbar.component.html',
    styleUrls: ['./toolbar.component.css']
})
export class ToolbarComponent implements OnInit {

    isCollapsed: boolean;

    constructor(private _userService: UsersService) { }

    ngOnInit() { }

    goToTop() {
        window.scroll(0, 0);
    }

    disconnect() {
        this._userService.disconnect();
    }
}
