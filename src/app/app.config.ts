import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
//const environment = require('../../../environments/environment.json');


@Injectable()
export class AppConfig {

    private config: Object = null;
    private env:    any = null;
    private envLoc: String = "env/";

    constructor(private _http: HttpClient) {

    }

    /**
     * Use to get the data found in the second file (config file)
     */
    public getConfig(key: any) {
        return this.config[key];
    }

    /**
     * Use to get the data found in the first file (env file)
     */
    public getEnv(key: any) {
        return this.env[key];
    }

    /**
     * This method:
     *   a) Loads "env.json" to get the current working environment (e.g.: 'production', 'development')
     *   b) Loads "config.[env].json" to get all env's variables (e.g.: 'config.development.json')
     */
    public load() {
        return new Promise((resolve, reject) => {
            this._http.get(this.envLoc+'env.json').subscribe( (envResponse) => {
                this.env = envResponse;
                let request:any = null;

                switch (this.env.env) {
                    case 'production': {
                        request = this._http.get(this.envLoc+'config.' + this.env.env + '.json');
                    } break;

                    case 'development': {
                        request = this._http.get(this.envLoc+'config.' + this.env.env + '.json');
                    } break;

                    case 'default': {
                        console.error('Environment file is not set or invalid');
                        resolve(true);
                    } break;
                }

                if (request) {
                    request.subscribe((responseData) => {
                            this.config = responseData;
                            resolve(true);
                        });
                } else {
                    console.error('Env config file "env.json" is not valid');
                    resolve(true);
                }
            });

        });
    }
}