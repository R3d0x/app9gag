import { Component, OnInit, OnDestroy, HostListener, Directive } from '@angular/core';
import { ImagesService } from '../services/images/images.service';
import { VotesService } from '../services/votes/votes.service';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-posts',
    templateUrl: './posts.component.html',
    styleUrls: ['./posts.component.css']
})

@Directive({
    selector: '[scrollTracker]'
})
export class PostsComponent implements OnInit {

    images = [];
    currentPage: any = 0;
    sortProperty: string = null;
    imagesSub;
    queryParamsSub;

    constructor(private route: ActivatedRoute, private _imagesService: ImagesService) { }

    ngOnInit() {
        this.route.queryParams.subscribe(() => {
            this.currentPage = 0;
            if (document.documentElement.scrollTop) {
                document.documentElement.scrollTop = 0;
            }
            if (document.body.scrollTop) {
                document.body.scrollTop = 0;
            }
        })
        this.queryParamsSub = this.route.queryParams
            .subscribe(({ sort }) => {
                this.sortProperty = (sort ? sort : '');
                this.imagesSub = this._imagesService.fetchImages(this.currentPage, this.sortProperty)
                    .subscribe((images) => { this.images = images['content']; });
            });
    }

    @HostListener('window:scroll', [])
    onScroll() {
        const pos = (document.documentElement.scrollTop || document.body.scrollTop) + document.documentElement.offsetHeight;
        const max = document.documentElement.scrollHeight;
        if (pos === max) {
            this.currentPage += 1;
            this._imagesService.fetchImages(this.currentPage, this.sortProperty)
                .subscribe((images) => this.images = [...this.images, ...images['content']]);
        }
    }

    removeImage(id) {
        this.images = this.images.filter(image => image.id !== id);
    }

    ngOnDestroy() {
        this.queryParamsSub.unsubscribe();
        this.imagesSub.unsubscribe();
    }
}
