import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ImagesService } from '../services/images/images.service';
import { VotesService } from '../services/votes/votes.service';
import { CommentsService } from '../services/comments/comments.service';
import { UsersService } from '../services/users/users.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-post-details',
    templateUrl: './post-details.component.html',
    styleUrls: ['./post-details.component.css']
})
export class PostDetailsComponent implements OnInit {

    alreadyVoted: boolean;
    imageDetails: any;
    score;
    comments = [];

    constructor(private route: ActivatedRoute,
        private _imagesService: ImagesService,
        private _votesService: VotesService,
        private _commentsService: CommentsService,
        private _userService: UsersService,
        private _imageService: ImagesService,
        private _router: Router) { }

    ngOnInit() {
        this.route.params.subscribe(params => {
            const id = params['id'];
            this.getImageDetails(id);
        });
    }

    getImageDetails(id) {
        this._imagesService.fetchImageDetails(id)
            .subscribe((data) => { this.imageDetails = data; });
        this._votesService.fetchImageScore(id)
            .subscribe((data) => { this.score = (data['score'] != null ? data['score'] : '0'); });
        this._commentsService.fetchImageComments(id)
            .subscribe((data) => { this.comments = data; });
    }

    sendComment(comment, reply) {
        document.getElementById('commentEntry')['value'] = '';
        this._commentsService.addComment(comment, this.imageDetails.id, reply, this._userService.getUserId())
            .subscribe((data) => { this.comments.unshift(data); });
    }

    downVote() {
        this._votesService.addVote(this.imageDetails.id, false, this._userService.getUserId())
            .subscribe((data) => { this.score--; });
    }

    upVote() {
        this._votesService.addVote(this.imageDetails.id, true, this._userService.getUserId())
            .subscribe((data) => { this.score++; });
    }

    deletePost() {
        if (this.isUserPostAuthor()) {
            this._commentsService.deleteCommentsOnImage(this.imageDetails.id)
                .subscribe();
            this._votesService.deleteVotesOnImage(this.imageDetails.id)
                .subscribe();
            this._imageService.deleteImage(this.imageDetails.id).then(() => {
                this._router.navigate(['/']);
            });
        }
    }

    isUserPostAuthor() {
        return this._userService.logged && this.imageDetails.idUtilisateur === this._userService.getUserId();
    }
}
