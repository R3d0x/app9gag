import { Component, OnInit, Output, Input } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { ImagesService } from '../services/images/images.service';
import { UsersService } from '../services/users/users.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-upload',
    templateUrl: './upload.component.html',
    styleUrls: ['./upload.component.css']
})
export class UploadComponent implements OnInit {

    form: FormGroup;
    image: any;

    constructor(private _imagesService: ImagesService, private _userService: UsersService, private _router: Router) {
        this.form = new FormGroup({
            pseudo: new FormControl(),
            titreImg: new FormControl()
        });
    }

    ngOnInit() {
        if (!this._userService.logged) {
            window.location.href = '/';
        }
        document.addEventListener('paste', (event) => {
            for (const item of event['clipboardData'].items) {
                if (item.kind === 'file') {
                    this.loadImage(item.getAsFile());
                }
            }
        });
        // this._usersService.login('alice', 'alice12');
    }

    loadImage(file: File) {
        const myReader: FileReader = new FileReader();
        const preview = document.getElementById('preview');

        myReader.onloadend = (e) => {
            this.image = myReader.result;

            preview['src'] = myReader.result;
            preview.style.display = '';
        };
        myReader.readAsDataURL(file);

    }

    changeListener($event): void {
        this.readThis($event.target);
    }

    readThis(inputValue: any): void {
        this.loadImage(inputValue.files[0]);
    }

    submit(form: any) {
        this._imagesService.addImage(form, this.image)
            .subscribe((data) => {
                this._router.navigate(['/images/' + data.id]);
            });
    }
}
