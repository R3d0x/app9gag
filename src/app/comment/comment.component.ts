import { Component, OnInit, Input } from '@angular/core';
import { UsersService } from '../services/users/users.service';
import { CommentsService } from '../services/comments/comments.service';
import { PostDetailsComponent } from '../post-details/post-details.component';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.css']
})
export class CommentComponent implements OnInit {

  public author: string;

  @Input('comment') commentDetails;
  @Input() postDetails: PostDetailsComponent;
  constructor(private _userService: UsersService, private _commentsService: CommentsService, private postDetail: PostDetailsComponent) { }

  ngOnInit() {
    this._userService.getUsernameById(this.commentDetails.userId).then((username) => {
      this.author = username;
    });
  }

  deleteComment(commentToDeleteId) {
    this._commentsService.deleteComment(this.postDetail.imageDetails.id, commentToDeleteId)
      .subscribe((data) => { this.postDetail.comments = this.postDetail.comments.filter(comment => comment.id !== commentToDeleteId); });
  }

  isUserCommentAuthor() {
    return this._userService.logged && this.commentDetails.userId === this._userService.getUserId();
  }

}
