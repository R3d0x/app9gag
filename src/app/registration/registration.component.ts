import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { UsersService } from '../services/users/users.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-registration',
    templateUrl: './registration.component.html',
    styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

    form: FormGroup;

    constructor(private _router: Router, private _usersService: UsersService) {
        this.form = new FormGroup({
            login: new FormControl(),
            password: new FormControl()
        });
    }

    ngOnInit() {
    }

    submit(form: any) {
        this._usersService.createUser(this.form.get('login').value, this.form.get('password').value);
        this._router.navigate(['/']);
    }
}
